package fraction_expression;

public class Arithmetic {
 
    private static final String DIVIDING_BY_ZERO = "Делить на нуль нельзя!";
   
    public static Fraction sum(Fraction a, Fraction b) {
       int denominator = getLCM(a.getDenominator(), b.getDenominator());
       int numerator = a.getNumerator() * (denominator / a.getDenominator());
       numerator += b.getNumerator() * (denominator / b.getDenominator());
       
       return new Fraction(numerator, denominator);
    }
    
    public static Fraction sub(Fraction a, Fraction b) {
        int denominator = getLCM(a.getDenominator(), b.getDenominator());
        int numerator = a.getNumerator() * (denominator / b.getDenominator());
        numerator -= b.getNumerator() * (denominator / b.getDenominator());
        
        return new Fraction(numerator, denominator);
    }
    
    public static Fraction multiple(Fraction a, Fraction b) {
        int numerator = a.getNumerator() * b.getNumerator();
        int denominator = a.getDenominator() * b.getDenominator();
        
    	return new Fraction(numerator, denominator);
    }
   
    public static Fraction divide(Fraction a, Fraction b) throws Exception {
    	if(b.isZero()) {
    		throw new Exception(DIVIDING_BY_ZERO);
    	}
    	
        return multiple(a, b.getReverse());
    }
    
    public static Fraction pow(Fraction a, Fraction b) {
        double grade = b.doubleValue();
        double numerator = Math.pow(a.getNumerator(), grade);
        double denominator = Math.pow(a.getDenominator(), grade);
         
        return new Fraction((int)numerator, (int)denominator);
    }
    
    public static int getGCD(int a, int b) {
        return b != 0 ? getGCD(b, a % b) : a;
    }
    
    public static int getLCM(int a, int b) {
        return Math.abs(a * b) / getGCD(a, b);
    }
}