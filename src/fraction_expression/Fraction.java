package fraction_expression;
public class Fraction {

    private int denominator, numerator;

    public Fraction(int numerator, int denominator) {
        this.numerator = numerator;
        this.denominator = denominator;
        simplify();
    }
    
    public Fraction(int whole, int numerator, int denominator) {
    	this(numerator + whole * denominator, denominator);
    }
    
    public Fraction() {
        this(0, 1);
    }

    private int getGCD(int a, int b) {
        return b != 0 ? getGCD(b, a % b) : a;
    }

    public void simplify() {
        int gcd = getGCD(Math.abs(numerator), Math.abs(denominator));
        numerator /= gcd;
        denominator /= gcd;
    }
 
    public double doubleValue() {
        return (double)numerator / denominator;
    }

    public int intValue() {
        return numerator / denominator;
    }

    public int getDenominator() {
        return denominator;
    }

    public int getNumerator() {
        return numerator;
    }

    public boolean isZero() {
        return numerator == 0 && denominator == 1;
    }

    public Fraction getReverse() {
        if(isZero())
            return new Fraction(numerator, denominator);
        
        return new Fraction(denominator, numerator);
    }
    
    public String toString() {
        int n = this.numerator, d = this.denominator;
        double whole = (double)n / d;
        
        if(whole % 1.0 == 0.0) {
            return Integer.toString((int)whole);
        }
        
        String decFraction = Double.toString(whole);
        
        if(decFraction.length() - decFraction.indexOf(".") < 4) {
            return decFraction; 
        }
        
        StringBuilder builder = new StringBuilder();
        
        if((int)whole != 0) {
            builder.append((int)whole);
        }
        
        builder.append("(").append(n % d).append("/").append(d).append(")");
        return builder.toString();
        }
    }